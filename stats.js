exports.Stats = function(playerStats, maxDraws) {
	let refinedStats = {}
	for (let i = Object.keys(playerStats).length - 1; i >= 0; i--) {
		let botName = Object.keys(playerStats)[i]
		let onePlayerStats = refinePlayerStats(botName, playerStats[botName], maxDraws)
		refinedStats[botName] = onePlayerStats
	}
	return refinedStats
}

function refinePlayerStats(botName, playerStats, maxDraws) {
	let refinedStats = {
		wins: playerStats.wins,
		actionPercentage: {
			S: 0,
			D: 0,
			A: 0
		},
		killers: {},
		totalRounds: 0,
		notTiedRounds: 0,
		averageRoundDuration: 0,
		roundDurationTypicalDeviation: 0
	}

	let lostDuels = 0
	for (let i = playerStats.history.length - 1; i >= 0; i--) {
		let h = playerStats.history[i]
		for (let j = Object.keys(h).length - 1; j >= 0; j--) {
			let enemyBotName = Object.keys(h)[j]
			let duels = h[enemyBotName]

			for (let l = 0; l < duels.length; l++) {
				let duel = duels[l]
				if (duel.winner != botName) {
					lostDuels++
					if (refinedStats.killers[duel.winner] == undefined) {
						refinedStats.killers[duel.winner] = 1
					} else {
						refinedStats.killers[duel.winner]++
					}
				}

				for (let k = 0; k < duel.ownHistory.length; k++) {
					let action = duel.ownHistory[k]
					refinedStats.actionPercentage[action]++
				}

				refinedStats.totalRounds++
				if (duel.ownHistory.length < maxDraws) {
					refinedStats.notTiedRounds++
					refinedStats.averageRoundDuration += duel.ownHistory.length
				}
			}
		}
	}

	refinedStats.averageRoundDuration = refinedStats.averageRoundDuration / refinedStats.notTiedRounds

	for (let i = playerStats.history.length - 1; i >= 0; i--) {
		let h = playerStats.history[i]
		for (let j = Object.keys(h).length - 1; j >= 0; j--) {
			let enemyBotName = Object.keys(h)[j]
			let duels = h[enemyBotName]
			for (let l = 0; l < duels.length; l++) {
				let duel = duels[l]
				if (duel.ownHistory.length < maxDraws) {
					refinedStats.roundDurationTypicalDeviation += Math.pow(duel.ownHistory.length - refinedStats.averageRoundDuration, 2)
				}
			}
		}
	}

	refinedStats.roundDurationTypicalDeviation = Math.sqrt(refinedStats.roundDurationTypicalDeviation / refinedStats.notTiedRounds).toFixed(2)
	refinedStats.averageRoundDuration = refinedStats.averageRoundDuration.toFixed(2)

	let totalActions = refinedStats.actionPercentage.S + refinedStats.actionPercentage.D + refinedStats.actionPercentage.A
	refinedStats.actionPercentage.S = (refinedStats.actionPercentage.S / totalActions * 100).toFixed(2) + '%'
	refinedStats.actionPercentage.D = (refinedStats.actionPercentage.D / totalActions * 100).toFixed(2) + '%'
	refinedStats.actionPercentage.A = (refinedStats.actionPercentage.A / totalActions * 100).toFixed(2) + '%'

	for (let j = Object.keys(refinedStats.killers).length - 1; j >= 0; j--) {
		let kName = Object.keys(refinedStats.killers)[j]
		refinedStats.killers[kName] = (refinedStats.killers[kName] / lostDuels * 100).toFixed(2) + '%'
	}

	return refinedStats
}


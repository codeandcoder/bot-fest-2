let Bot = require('./bot.js').Bot

let SHOOT = 'S'
let SUPERSHOOT = 'X'
let DODGE = 'D'
let AMMO  = 'A'

exports.FaceOff = function(bots, rounds, maxDraws, logs) {
	// Initialize bots
	let stats = {}
	let cBots = []
	for (let i = bots.length - 1; i >= 0; i--) {
		let b = bots[i]
		cBots.push(Bot(b[0], b[1]))
		stats[b[0]] = {
			wins : 0,
			history : {}
		}
	}

	for (let b1 = 0; b1 < cBots.length; b1++) {
		for (let b2 = b1+1; b2 < cBots.length; b2++) {
			let bot1 = cBots[b1]
			let bot2 = cBots[b2]
			for (let r = 0; r < rounds; r++) {
				bot1.reset()
				bot2.reset()

				let winner = null
				for (let i = 0; i < maxDraws && winner == null; i++) {
					winner = duel(bot1, bot2, logs)
				}

				if (logs) console.log('WINNER: ' + (winner == null ? 'None' : winner.botName))
				if (logs) console.log('-----------------------------------------------------')

				saveStats(stats, bot1, bot2, winner)
			}
		}
	}

	return stats
}

function duel(bot1, bot2, logs) {
	let winner = null
	let act1 = bot1.act(bot1.history, bot2.history)
	let act2 = bot2.act(bot2.history, bot1.history)
	act1 = act1 != SHOOT && act1 != AMMO && act1 != DODGE ? DODGE : act1
	act2 = act2 != SHOOT && act2 != AMMO && act2 != DODGE ? DODGE : act2

	bot1.history.push(act1)
	bot2.history.push(act2)

	if (logs) console.log(bot1.botName + ' act :' + act1)
	if (logs) console.log(bot2.botName + ' act :' + act2)
	if (logs) console.log('--------')

	act1 = act1 == SHOOT ? bot1.ammo >= 5 ? SUPERSHOOT : SHOOT : act1
	act2 = act2 == SHOOT ? bot2.ammo >= 5 ? SUPERSHOOT : SHOOT : act2

	winner = itKills(bot1, act1, bot2, act2) ? bot1 : itKills(bot2, act2, bot1, act1) ? bot2 : null

	// Ammo adjustments
	if (act1 == SHOOT || act1 == SUPERSHOOT) {
		bot1.ammo = bot1.ammo == 0 ? 0 : bot1.ammo-1
	} else if (act1 == AMMO) {
		bot1.ammo += 1
	}

	if (act2 == SHOOT || act2 == SUPERSHOOT) {
		bot2.ammo = bot2.ammo == 0 ? 0 : bot2.ammo-1
	} else if (act2 == AMMO) {
		bot2.ammo += 1
	}

	return winner
}

function itKills(bot, botAct, otherBot, otherBotAct) {
	if (botAct == SUPERSHOOT) {
		if (otherBotAct != SUPERSHOOT) {
			return true
		}
	} else if (botAct == SHOOT && bot.ammo > 0) {
		if (otherBotAct == AMMO || otherBotAct == SHOOT && otherBot.ammo <= 0) {
			return true
		}
	}

	return false
}

function saveStats(stats, bot1, bot2, winner) {
	// WINNER STATS
	if (winner != null) {
		stats[winner.botName].wins += 1
	}

	// BOT 1 STATS
	if (stats[bot1.botName].history[bot2.botName] == undefined) {
		stats[bot1.botName].history[bot2.botName] = []
	}
	stats[bot1.botName].history[bot2.botName].push({
		winner: winner == null ? 'None' : winner.botName,
		ownHistory: bot1.history,
		otherHistory: bot2.history
	})

	// BOT 2 STATS
	if (stats[bot2.botName].history[bot1.botName] == undefined) {
		stats[bot2.botName].history[bot1.botName] = []
	}
	stats[bot2.botName].history[bot1.botName].push({
		winner: winner == null ? 'None' : winner.botName,
		ownHistory: bot2.history,
		otherHistory: bot1.history
	})
}
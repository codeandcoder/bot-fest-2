exports.bot = function(ownHistory, otherHistory) {
	let round = ownHistory.length

	let action = round % 3

	return action == 0 ? 'A' : action == 1 ? 'B' : action == 2 ? 'S' : null
}
exports.bot = function(ownHistory, otherHistory) {
	let round = ownHistory.length

	if (round == 0) {
		return 'A'
	}

	let enemyAmmo = calculateAmmo(otherHistory)
	let ownAmmo = calculateAmmo(ownHistory)

	if (ownAmmo == 0) {
		return 'A'
	}

	if (enemyAmmo > 0) {
		return 'D'
	}

	return 'S'
}

function calculateAmmo(history) {
	let ammo = 0
	for (let i = 0; i < history.length; i++) {
		if (history[i] == 'A') {
			ammo++
		} else if (history[i] == 'S') {
			ammo = ammo == 0 ? 0 : ammo-1
		}
	}
	return ammo
}
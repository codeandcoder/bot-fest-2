exports.bot = function(ownHistory, otherHistory) {
	let round = ownHistory.length
	let enemyAmmo = calculateAmmo(otherHistory)
	let ownAmmo = calculateAmmo(ownHistory)
	let enemyLastMovement = otherHistory.length > 0 ? otherHistory[otherHistory.length-1] : 'S'
	let random = getRandomInt(0,100)

	if (enemyAmmo == 0) {
		return 'A'
	}

	if (ownAmmo >= 5) {
		return 'S'
	}

	if (enemyAmmo > 3 && ownAmmo > 0) {
		return 'S'
	}

	if (enemyLastMovement == 'S') {
		return random > 50 ? 'S' : 'A'
	} else if (enemyLastMovement == 'D') {
		return random > 20 ? 'D' : 'A'
	}
	
	return random > 50 ? 'D' : 'A'
}

function calculateAmmo(history) {
	let ammo = 0
	for (let i = 0; i < history.length; i++) {
		if (history[i] == 'A') {
			ammo++
		} else if (history[i] == 'S') {
			ammo = ammo == 0 ? 0 : ammo-1
		}
	}
	return ammo
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
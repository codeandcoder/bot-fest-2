exports.bot = function(ownHistory, otherHistory) {
	let round = ownHistory.length

	if (round == 0) {
		return 'A'
	}

	let enemyAmmo = calculateAmmo(otherHistory)
	let ownAmmo = calculateAmmo(ownHistory)

	let enemyLastMovement = otherHistory.length > 0 ? otherHistory[otherHistory.length-1] : 'S'

	if (enemyLastMovement == 'S') {
		if (ownAmmo > 0) {
			return 'S'
		} else {
			return 'A'
		}
	} else if (enemyLastMovement == 'D') {
		if (ownAmmo > 0) {
			return 'S'
		} else {
			return 'D'
		}
	}
	
	return 'D'
}

function calculateAmmo(history) {
	let ammo = 0
	for (let i = 0; i < history.length; i++) {
		if (history[i] == 'A') {
			ammo++
		} else if (history[i] == 'S') {
			ammo = ammo == 0 ? 0 : ammo-1
		}
	}
	return ammo
}
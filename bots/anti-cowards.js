exports.bot = function(ownHistory, otherHistory) {
	let round = ownHistory.length

	if (round == 0) {
		return 'A'
	}

	let enemyAmmo = calculateAmmo(otherHistory)
	let ownAmmo = calculateAmmo(ownHistory)

	if (ownAmmo >= 5) {
		return 'S'
	}

	if (enemyAmmo == 0) {
		return 'A'
	}

	let random = getRandomInt(0,100)

	return random > 80 ? 'D' : random > 70 ? 'S' : 'A'
}

function calculateAmmo(history) {
	let ammo = 0
	for (let i = 0; i < history.length; i++) {
		if (history[i] == 'A') {
			ammo++
		} else if (history[i] == 'S') {
			ammo = ammo == 0 ? 0 : ammo-1
		}
	}
	return ammo
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
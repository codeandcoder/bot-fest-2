exports.Bot = function(botName, act) {
	let self = {}

	self.botName = botName
	self.act = act
	self.history = []
	self.ammo = 0

	self.reset = function() {
		self.history = []
		self.ammo = 0
	}

	return self
}
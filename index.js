let FaceOff = require('./face-off.js').FaceOff
let Stats = require('./stats.js').Stats

let bots = []
bots.push(['Evolution Child', require('./bots/evolution-child').bot])
bots.push(['Crazy Two', require('./bots/crazy-two').bot])
bots.push(['Simple Dummy', require('./bots/simple-dummy').bot])
bots.push(['Random', require('./bots/random').bot])
bots.push(['Copycat', require('./bots/copycat').bot])
bots.push(['Coward', require('./bots/coward').bot])
bots.push(['Coward v2', require('./bots/coward-v2').bot])
bots.push(['Crazy One', require('./bots/crazy-one').bot])
bots.push(['Anti-Cowards', require('./bots/anti-cowards').bot])
bots.push(['Relaxed Anti-Cowards', require('./bots/relaxed-anti-cowards').bot])
bots.push(['Fast Win', require('./bots/fast-win').bot])

let logs = false
let repetitions = 1000
let rounds = 3
let maxDraws = 100

// CHAMPIONSHIP
let playerStats = {}
for (let i = 0; i < repetitions; i++) {
	let stats = FaceOff(bots, rounds, maxDraws, logs)
	for (var j = Object.keys(stats).length - 1; j >= 0; j--) {
		let botName = Object.keys(stats)[j]
		if (playerStats[botName] != undefined) {
			playerStats[botName].wins += stats[botName].wins
			playerStats[botName].history.push(stats[botName].history)
		} else {
			playerStats[botName] = {wins: stats[botName].wins, history: [stats[botName].history]}
		}
	}
}

// RANKING
let ranking = []
for (var j = Object.keys(playerStats).length - 1; j >= 0; j--) {
	let botName = Object.keys(playerStats)[j]
	ranking.push({botName: botName, wins: (playerStats[botName].wins / (repetitions * rounds * (bots.length-1)) * 100).toFixed(2) + '%'})
}
ranking.sort(function compare(a,b) {
	let aPoints = parseFloat(a.wins.substring(0,a.wins.indexOf('%')))
	let bPoints = parseFloat(b.wins.substring(0,b.wins.indexOf('%')))
  if (aPoints > bPoints)
    return -1;
  if (aPoints < bPoints)
    return 1;
  return 0;
})
console.log(ranking)

//STATS
let refinedStats = Stats(playerStats, maxDraws)
console.log(JSON.stringify(refinedStats))